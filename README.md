# RPG game
## Description
This application allows you to create a character of one of four different classes.
The different classes have different stats and types of weapons and armor they can use. 
The characters' stats change based on the items (weapons and armor) they have equipped. 
Their stats also change based on their level.

## Usage
Once a character of a class is created they can equip items with the equip(Item item) method after initializing the item to be equipped. 
To level up your character you simply call the levelUp() method.