package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void TestInitialLvl_lvlIs1_ShouldPass() {
        Mage mage = new Mage("Houdini");
        int lvl = mage.getLvl();
        assertEquals(1, lvl);
    }

    @Test
    void TestLvlUp_lvlIncreases_ShouldPass() {
        Mage mage = new Mage("Houdini");
        mage.levelUp();
        int lvl = mage.getLvl();
        assertEquals(2, lvl);
    }

    @Test
    void TestMageAttributes_initialBaseAsExpected_ShouldPass() {
        Mage character = new Mage("Houdini");
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(1, 1, 8, 5);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestRangerAttributes_initialBaseAsExpected_ShouldPass() {
        Ranger character = new Ranger("Robin Hood");
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(1, 7, 1, 8);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestRogueAttributes_initialBaseAsExpected_ShouldPass() {
        Rogue character = new Rogue("Lara");
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(2, 6, 1, 8);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestWarriorAttributes_initialBaseAsExpected_ShouldPass() {
        Warrior character = new Warrior("Kratos");
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(5, 2, 1, 10);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestMageLevelUp_leveledUpBaseAsExpected_ShouldPass() {
        Mage character = new Mage("Houdini");
        character.levelUp();
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(2, 2, 13, 8);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestRangerLevelUp_leveledUpBaseAsExpected_ShouldPass() {
        Ranger character = new Ranger("Robin Hood");
        character.levelUp();
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(2, 12, 2, 10);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestRogueLevelUp_leveledUpBaseAsExpected_ShouldPass() {
        Rogue character = new Rogue("Lara");
        character.levelUp();
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(3, 10, 2, 11);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }

    @Test
    void TestWarriorLevelUp_leveledUpBaseAsExpected_ShouldPass() {
        Warrior character = new Warrior("Kratos");
        character.levelUp();
        PrimaryAttribute characterAttributes = character.getBaseStats();
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(8, 4, 2, 15);
        boolean isEqual = characterAttributes.equals(expectedAttributes);
        assertTrue(isEqual);
    }
}