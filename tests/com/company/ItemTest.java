package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void TestEquip_TooHighWeaponLvl_ShouldThrowInvalidWeaponException() {
        Weapon weapon = new Weapon("Common axe", 2, 7, 1.1, Weapon.WeaponTypes.AXE);
        Warrior warrior = new Warrior("Kratos");
        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            warrior.equip(weapon);
        });
    }


    @Test
    void TestEquip_TooHighArmorLvl_ShouldThrowInvalidArmorException() {
        Armor armor = new Armor("Common Plate Body Armor", Slot.BODY, 2, new PrimaryAttribute(1, 0, 0, 2), Armor.ArmorTypes.PLATE);
        Warrior warrior = new Warrior("Kratos");
        Assertions.assertThrows(InvalidArmorException.class, () -> {
            warrior.equip(armor);
        });
    }

    @Test
    void TestEquip_WrongWeaponType_ShouldThrowInvalidWeaponException() {
        Weapon weapon = new Weapon("Common bow", 1, 12, 0.8, Weapon.WeaponTypes.BOW);
        Warrior warrior = new Warrior("Kratos");
        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            warrior.equip(weapon);
        });
    }

    @Test
    void TestEquip_WrongArmorType_ShouldThrowInvalidArmorException() {
        Armor armor = new Armor("Common Cloth Head Armor", Slot.HEAD, 1, new PrimaryAttribute(0, 0, 5, 1), Armor.ArmorTypes.CLOTH);
        Warrior warrior = new Warrior("Kratos");
        Assertions.assertThrows(InvalidArmorException.class, () -> {
            warrior.equip(armor);
        });
    }

    @Test
    void TestEquip_EquipValidWeapon_ShouldPass(){
        Weapon weapon = new Weapon("Common axe", 1, 7, 1.1, Weapon.WeaponTypes.AXE);
        Warrior warrior = new Warrior("Kratos");
        boolean equipSucceeded = false;
        try {
            equipSucceeded = warrior.equip(weapon);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            return;
        }
        assertTrue(equipSucceeded);
    }

    @Test
    void TestEquip_EquipValidArmor_ShouldPass(){
        Armor armor = new Armor("Common Plate Body Armor", Slot.BODY, 1, new PrimaryAttribute(1, 0, 0, 2), Armor.ArmorTypes.PLATE);
        Warrior warrior = new Warrior("Kratos");
        boolean equipSucceeded = false;
        try {
            equipSucceeded = warrior.equip(armor);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            return;
        }
        assertTrue(equipSucceeded);
    }

    @Test
    void TestCharacterDps_DpsWithoutWeapon_ShouldPass(){
        Warrior warrior = new Warrior("Kratos");
        double characterDps = warrior.getDps();
        double expected = 1*(1+(5/100));
        assertEquals(expected, characterDps);
    }

    @Test
    void TestCharacterDps_DpsWithWeapon_ShouldPass(){
        Warrior warrior = new Warrior("Kratos");
        Weapon weapon = new Weapon("Common axe", 1, 7, 1.1, Weapon.WeaponTypes.AXE);
        try {
            warrior.equip(weapon);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            return;
        }
        double characterDps = warrior.getDps();
        double expected = (7*1.1)*(1+(5/100));
        assertEquals(expected, characterDps);
    }

    @Test
    void TestCharacterDps_DpsWithWeaponAndArmor_ShouldPass(){
        Warrior warrior = new Warrior("Kratos");
        Weapon weapon = new Weapon("Common axe", 1, 7, 1.1, Weapon.WeaponTypes.AXE);
        Armor armor = new Armor("Common Plate Body Armor", Slot.BODY, 1, new PrimaryAttribute(1, 0, 0, 2), Armor.ArmorTypes.PLATE);
        try {
            warrior.equip(weapon);
            warrior.equip(armor);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            return;
        }
        double characterDps = warrior.getDps();
        double expected = (7*1.1)*(1+((5+1)/100));
        assertEquals(expected, characterDps);
    }

}