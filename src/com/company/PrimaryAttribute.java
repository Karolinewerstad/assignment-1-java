package com.company;

public class PrimaryAttribute {

    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    public PrimaryAttribute(int strength, int dexterity, int intelligence, int vitality){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }

    protected void updateStats(PrimaryAttribute change){
        this.strength += change.strength;
        this.dexterity += change.dexterity;
        this.intelligence += change.intelligence;
        this.vitality += change.vitality;
    }

    protected void replicateStats(PrimaryAttribute setTo) {
        this.strength = setTo.strength;
        this.dexterity = setTo.dexterity;
        this.intelligence = setTo.intelligence;
        this.vitality = setTo.vitality;
    }

    public int getStat(String stat) {
        switch(stat) {
            case "strength":
                return this.strength;
            case "dexterity":
                return this.dexterity;
            case "intelligence":
                return this.intelligence;
            case "vitality":
                return this.vitality;
            default:
                return 0;

        }

    }
    //Overriding equals method so a Primary attribute object equals another if they have the same stats
    public boolean equals(PrimaryAttribute other) {
        if (other == null) {
            return false;
        }
        boolean equalsStrength = false;
        boolean equalsDexterity = false;
        boolean equalsIntelligence = false;
        boolean equalsVitality = false;

        if (this.strength == other.getStat("strength")) {
            equalsStrength = true;
        }
        if (this.dexterity == other.getStat("dexterity")) {
            equalsDexterity = true;
        }
        if (this.intelligence == other.getStat("intelligence")) {
            equalsIntelligence = true;
        }
        if (this.vitality == other.getStat("vitality")) {
            equalsVitality = true;
        }
        if(equalsStrength && equalsDexterity && equalsIntelligence && equalsVitality) {
            return true;
        }
        return false;
    }
}
