package com.company;

public class Weapon extends Item {
    private int damage;
    private double attacksSpeed, dps;
    private WeaponTypes type;

    public enum WeaponTypes {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    public Weapon(String name, int lvl, int damage, double attackSpeed, WeaponTypes type) {
        super(name, Slot.WEAPON, lvl);
        this.damage = damage;
        this.attacksSpeed = attackSpeed;
        this.dps = damage * attackSpeed;
        this.type = type;
    }

    public double getDps() {
        return this.dps;
    }

    public WeaponTypes getType(){
        return this.type;
    }

}
