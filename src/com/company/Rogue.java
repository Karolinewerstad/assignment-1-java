package com.company;

public class Rogue extends Character{

    public Rogue(String name){

        super(name, "dexterity");
        this.baseStats = new PrimaryAttribute(2, 6, 1, 8);
        this.totalStats = new PrimaryAttribute(2, 6, 1, 8);
        this.validWeapons = new Weapon.WeaponTypes[]{Weapon.WeaponTypes.DAGGER, Weapon.WeaponTypes.SWORD};
        this.validArmor = new Armor.ArmorTypes[]{Armor.ArmorTypes.LEATHER, Armor.ArmorTypes.MAIL};
    }

    public void levelUp() {
        this.baseStats.updateStats(new PrimaryAttribute(1, 4, 1, 3));
        this.lvl += 1;
    }
}
