package com.company;

public class Warrior extends Character{

    public Warrior(String name){
        super(name, "strength");
        this.baseStats = new PrimaryAttribute(5, 2, 1, 10);
        this.totalStats = new PrimaryAttribute(5, 2, 1, 10);
        this.validWeapons = new Weapon.WeaponTypes[]{Weapon.WeaponTypes.AXE, Weapon.WeaponTypes.HAMMER, Weapon.WeaponTypes.SWORD};
        this.validArmor = new Armor.ArmorTypes[]{Armor.ArmorTypes.MAIL, Armor.ArmorTypes.PLATE};
    }

    public void levelUp() {
        this.baseStats.updateStats(new PrimaryAttribute(3, 2, 1, 5));
        this.lvl += 1;
    }
}
