package com.company;

public class Main {

    public static void main(String[] args) {
        Warrior warrior = new Warrior("Kratos");
        Weapon weapon = new Weapon("Common axe", 1, 7, 1.1, Weapon.WeaponTypes.AXE);
        try {
            warrior.equip(weapon);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            System.out.println("Ooops");
        }
        CharacterWriter.writeStats(warrior);
    }
}
