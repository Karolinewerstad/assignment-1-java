package com.company;

public class Mage extends Character{

    public Mage(String name){
        super(name, "intelligence");
        this.baseStats = new PrimaryAttribute(1, 1, 8, 5);
        this.totalStats = new PrimaryAttribute(1, 1, 8, 5);
        this.validWeapons = new Weapon.WeaponTypes[]{Weapon.WeaponTypes.STAFF, Weapon.WeaponTypes.WAND};
        this.validArmor = new Armor.ArmorTypes[]{Armor.ArmorTypes.CLOTH};
    }

    public void levelUp() {
        this.baseStats.updateStats(new PrimaryAttribute(1, 1, 5, 3));
        this.lvl += 1;
    }
}
