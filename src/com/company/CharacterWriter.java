package com.company;

public class CharacterWriter {
    //Using a StringBuilder to get a beautiful character sheet and printing it.
    public static void writeStats(Character character) {
        StringBuilder characterSheet = new StringBuilder("Character name: ");
        characterSheet.append(character.getName() + "\n");
        characterSheet.append("Character level: ");
        characterSheet.append(character.getLvl() + "\n");
        PrimaryAttribute attributes = character.getTotalStats();
        characterSheet.append("Strength: ");
        characterSheet.append(attributes.getStat("strength") + "\n");
        characterSheet.append("Dexterity: ");
        characterSheet.append(attributes.getStat("dexterity") + "\n");
        characterSheet.append("Intelligence: ");
        characterSheet.append(attributes.getStat("intelligence") + "\n");
        characterSheet.append("Vitality: ");
        characterSheet.append(attributes.getStat("vitality") + "\n");
        characterSheet.append("DPS: ");
        characterSheet.append(String.format("%.2f", character.getDps()));
        System.out.println(characterSheet);
    }
}
