package com.company;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class Character {
    protected String name;
    protected int lvl = 1;
    protected PrimaryAttribute baseStats;
    protected PrimaryAttribute totalStats;
    protected HashMap<Slot, Item> equipment = new HashMap<>();
    protected double dps = 0;
    //Holds the string for the main attribute for the character (intelligence for mage etc.)
    protected String mainStat;
    protected Weapon.WeaponTypes[] validWeapons;
    protected Armor.ArmorTypes[] validArmor;

    public Character(String name, String mainStat) {
        this.name = name;
        this.mainStat = mainStat;
    }

    public boolean equip(Item item) throws InvalidWeaponException, InvalidArmorException {
        if (item.getSlot() == Slot.WEAPON) {
            equipWeapon((Weapon) item);
        } else {
            equipArmor((Armor) item);
        }
        return true;
    }

    private void calculateTotalStats() {
        this.totalStats.replicateStats(baseStats);
        for (Map.Entry<Slot, Item> entry : equipment.entrySet()) {
            if (entry.getKey() != Slot.WEAPON) {
                Armor armor = (Armor) entry.getValue();
                if (armor != null) {
                    this.totalStats.updateStats(armor.getStats());
                }
            }
        }
    }

    private void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        boolean isValid = false;
        //Check if the weapon is of one of the character's valid weapon types.
        for (int i = 0; i < this.validWeapons.length; i++) {
            if (weapon.getType() == this.validWeapons[i]) {
                isValid = true;
            }
        }
        if (!isValid) {
            throw new InvalidWeaponException(this.getClass().getName() + "s can not equip weapons of this type.");
        }
        //If the character's level is lower than the required level for the weapon, exception is thrown
        if (this.lvl < weapon.getRequiredLvl()) {
            throw new InvalidWeaponException("Your level is not high enough for this weapon.");
        }
        double weaponDps = weapon.getDps();
        int totalMainStat = totalStats.getStat(mainStat);
        this.dps = weaponDps*(1 + totalMainStat/100);
        weapon.equip(equipment);
    }

    private void equipArmor(Armor armor) throws InvalidArmorException {
        boolean isValid = false;
        //Check if the armor is of one of the character's valid armor types.
        for (int i = 0; i < this.validArmor.length; i++) {
            if (armor.getType() == this.validArmor[i]) {
                isValid = true;
            }
        }
        if (!isValid) {
            throw new InvalidArmorException(this.getClass().getName() + "s can not equip armor of this type.");
        }
        //If the character's level is lower than the required level for the weapon, exception is thrown
        if (this.lvl < armor.getRequiredLvl()) {
            throw new InvalidArmorException("Your level is not high enough for this armor.");
        }
        armor.equip(equipment);
        calculateTotalStats();
    }

    public abstract void levelUp();

    public String getName() {
        return name;
    }

    public int getLvl() {
        return lvl;
    }

    public PrimaryAttribute getBaseStats() {
        return baseStats;
    }

    public PrimaryAttribute getTotalStats() {
        return totalStats;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public double getDps() {
        //If this method is used before dps has been updated the first time, it is calculated here
        if (this.dps == 0) {
            this.dps = 1*(1+(this.baseStats.getStat(mainStat)/100));
        }
        return this.dps;
    }
}
