package com.company;

import java.util.HashMap;

public abstract class Item {
    protected String name;
    protected Slot slot;
    protected int requiredLvl;
    protected Enum type;

    public Item(String name, Slot slot, int lvl) {
        this.name = name;
        this.slot = slot;
        this.requiredLvl = lvl;
    }

    protected void equip(HashMap<Slot, Item> equipment) {
        equipment.put(slot, this);
    }

    public Slot getSlot(){
        return this.slot;
    }

    public int getRequiredLvl() {
        return this.requiredLvl;
    }
}
