package com.company;

public class Ranger extends Character {

    public Ranger(String name){
        super(name, "dexterity");
        this.baseStats = new PrimaryAttribute(1, 7, 1, 8);
        this.totalStats = new PrimaryAttribute(1, 7, 1, 8);
        this.validWeapons = new Weapon.WeaponTypes[]{Weapon.WeaponTypes.BOW};
        this.validArmor = new Armor.ArmorTypes[]{Armor.ArmorTypes.LEATHER, Armor.ArmorTypes.MAIL};
    }

    public void levelUp() {
        this.baseStats.updateStats(new PrimaryAttribute(1, 5, 1, 2));
        this.lvl += 1;
    }
}
