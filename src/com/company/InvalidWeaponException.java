package com.company;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String errorMessage) {
        super(errorMessage);
    }
}
