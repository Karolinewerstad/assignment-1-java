package com.company;

public class Armor extends Item {

    private PrimaryAttribute stats;
    private ArmorTypes type;

    enum ArmorTypes {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    public Armor(String name, Slot slot, int lvl, PrimaryAttribute stats, ArmorTypes type) {
        super(name, slot, lvl);
        this.stats = stats;
        this.type = type;
    }

    public PrimaryAttribute getStats() {
        return this.stats;
    }

    public ArmorTypes getType() {
        return this.type;
    }
}
